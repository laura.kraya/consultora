package consultcalificaciones;

public abstract class Reporte implements Promediable {
	private final static int CANT_CALIFICACIONES = 10;
	private String codigo;
	private double[] calificaciones;
	
	public Reporte(String codigo, double[] calificaciones) {
		this.codigo = codigo;
		this.calificaciones = calificaciones;
	}
	
	public static int getCantCalificaciones() {
		return CANT_CALIFICACIONES;
	}

	public abstract Resultado getResultado();
	
	public double getCalificacion(int pos) {
		if (pos < 0 || pos >= this.calificaciones.length) {
			throw new RuntimeException("Posición incorrecta");
		}
		return this.calificaciones[pos];
	}
	
	public double getPromedio() {
		double suma = 0;
		for (int i = 0; i < this.calificaciones.length; i++) {
			  suma += this.calificaciones[i];
			}
		return suma / Reporte.getCantCalificaciones();
	}
	
	public String getCodigo() {
		return this.codigo;
	};
}
