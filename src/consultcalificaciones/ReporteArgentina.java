package consultcalificaciones;

public class ReporteArgentina extends Reporte {
	private final static double NOTA_TOPE_0 = 1;
	private final static double NOTA_TOPE_1 = 4;
	private final static double NOTA_TOPE_2 = 6;
	private final static double NOTA_TOPE_3 = 8;
	private final static double NOTA_TOPE_4 = 10;
	
	public ReporteArgentina(String codigo, double[] calificaciones) {
		super(codigo, calificaciones);
	}

	@Override
	public Resultado getResultado() {
		
		Resultado resultado;
		double promedio = this.getPromedio();
		
		if(promedio < NOTA_TOPE_0) {
			resultado = Resultado.INVALIDO;
		} else if (promedio < NOTA_TOPE_1) {
			resultado = Resultado.INSUFICIENTE;
		} else if (promedio < NOTA_TOPE_2) {
			resultado = Resultado.ACEPTABLE;
		} else if (promedio < NOTA_TOPE_3) {
			resultado = Resultado.MUY_BUENO;
		} else if (promedio <= NOTA_TOPE_4) {
			resultado = Resultado.EXCELENTE;
		} else {
			resultado = Resultado.INVALIDO;
		}
		
		return resultado;
		
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
