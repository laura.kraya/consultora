package consultcalificaciones;

public class ReporteColombia extends Reporte {
	private boolean esAmazonico;
	private final static double NOTA_TOPE_0 = 0;
	private final static double NOTA_TOPE_1 = 3;
	private final static double NOTA_TOPE_2 = 3.8;
	private final static double NOTA_TOPE_3 = 4.5;
	private final static double NOTA_TOPE_4 = 5;

	public ReporteColombia(String codigo, double[] calificaciones, boolean esAmazonico) {
		super(codigo, calificaciones);
		this.esAmazonico = esAmazonico;
	}
	
	public boolean getEsAmazonico() {
		return this.esAmazonico;
	}

	@Override
	public Resultado getResultado() {
		
		Resultado resultado;
		double promedio = this.getPromedio();
		
		if(!esAmazonico) {
			if(promedio < NOTA_TOPE_0) {
				resultado = Resultado.INVALIDO;
			} else if (promedio < NOTA_TOPE_1) {
				resultado = Resultado.INSUFICIENTE;
			} else if (promedio < NOTA_TOPE_2) {
				resultado = Resultado.ACEPTABLE;
			} else if (promedio < NOTA_TOPE_3) {
				resultado = Resultado.MUY_BUENO;
			} else if (promedio <= NOTA_TOPE_4) {
				resultado = Resultado.EXCELENTE;
			} else {
				resultado = Resultado.INVALIDO;
			}
		} else {
			if(promedio < NOTA_TOPE_0) {
				resultado = Resultado.INVALIDO;
			} else if (promedio < NOTA_TOPE_1) {
				resultado = Resultado.ACEPTABLE;
			} else if (promedio < NOTA_TOPE_2) {
				resultado = Resultado.MUY_BUENO;
			} else if (promedio < NOTA_TOPE_4) {
				resultado = Resultado.EXCELENTE;
			} else {
				resultado = Resultado.INVALIDO;
			}
		}
		
		return resultado;
		
	}

	@Override
	public String toString() {
		return super.toString() + ", es Amazónico: " + this.esAmazonico;
	}	

}
