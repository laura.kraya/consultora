package consultcalificaciones;

public interface Promediable {
	public double getPromedio();
}
