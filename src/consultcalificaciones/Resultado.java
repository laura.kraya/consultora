package consultcalificaciones;

public enum Resultado {
	INVALIDO, INSUFICIENTE, ACEPTABLE, MUY_BUENO, EXCELENTE;
}
