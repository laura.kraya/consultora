package consultcalificaciones;

import java.util.ArrayList;

public class Consultora {
	private ArrayList<Reporte> reportes;

	public Consultora() {
		this.reportes = new ArrayList<Reporte>();
		this.cargarReportes();
	}
	
	public void mostrarCantidadesPorResultado() {
		int[] cantidades = {0, 0, 0, 0, 0};
		Resultado resultado;
		for (Reporte reporte: reportes) {
			resultado = reporte.getResultado();
			if(resultado == Resultado.INVALIDO) {
				cantidades[0]++;
			} else if (resultado == Resultado.INSUFICIENTE) {
				cantidades[1]++;
			} else if (resultado == Resultado.ACEPTABLE) {
				cantidades[2]++;
			} else if (resultado == Resultado.MUY_BUENO) {
				cantidades[3]++;
			} else if (resultado == Resultado.EXCELENTE) {
				cantidades[4]++;
			}
		}
		System.out.println("Resultado" + "\t" + "Cantidad\n" + "Inválido" + "\t" + cantidades[0] + "\nInsuficiente" + "\t" +
				cantidades[1] + "\nAceptable" + "\t" + cantidades[2] + "\nMuy Bueno" + "\t" + cantidades[3] + "\nExcelente" +
				"\t" + cantidades[4]);
	}
	
	private double[][] matrizDeCalificaciones() {
		/* TODO Devolver una matriz con las calificaciones
		 * donde cada fila representa un reporte y cada 
		 * columna es una calificaci�n de dicho reporte
		 */
		return null;
	}
	
	private String[] listaDeCodigos() {
		// TODO Devolver un array con los c�digos de reporte
		return null;
	}
	
	public void mostrarTablaDeCalificaciones() {
		String[] codigos = listaDeCodigos();
		double[][] califs = matrizDeCalificaciones();
		// La matriz se mostrar� en forma de tabla
		System.out.println("Tabla de calificaciones");
		for (int f = 0; f < califs.length; f++) {
			System.out.print(codigos[f] + "\t");
			for (int c = 0; c < califs[f].length; c++) {
				System.out.print(califs[f][c] + "\t");
			}
			System.out.println(); // Siguiente fila
		}
	}
	
	public void mostrarCantidadDeCalificacionesAmazonicas() {
		int cantidadAmazonicos = 0;
		for (Reporte reporte : reportes) {
			if(reporte instanceof ReporteColombia) {
				ReporteColombia repCol = (ReporteColombia) reporte;
				if(repCol.getEsAmazonico()) {
					cantidadAmazonicos++;
				}
			}
		}
		System.out.println("Cantidad de calificaciones Amaz�nicas: " + cantidadAmazonicos);
	}
	
	private boolean getEsAmazonico() {
		// TODO Auto-generated method stub
		return false;
	}

	///////////////////////// NO TOCAR /////////////////////////////////
	private void cargarReportes() {
		this.reportes.add(new ReporteArgentina("ARG-2-830", new double[] {5,1,8,6,5,10,3,8,8,9}));
		this.reportes.add(new ReporteArgentina("ARG-3-020", new double[] {3,9,10,8,3,7,5,1,3,4}));
		this.reportes.add(new ReporteArgentina("ARG-3-748", new double[] {10,4,1,9,3,8,10,1,10,8}));
		this.reportes.add(new ReporteArgentina("ARG-2-404", new double[] {4,6,4,4,6,5,8,1,1,1}));
		this.reportes.add(new ReporteArgentina("ARG-2-503", new double[] {4,5,5,9,7,3,9,6,1,5}));
		this.reportes.add(new ReporteArgentina("ARG-3-057", new double[] {8,6,7,1,8,4,9,7,2,1}));
		this.reportes.add(new ReporteArgentina("ARG-3-098", new double[] {8,7,7,1,8,9,2,4,1,7}));
		this.reportes.add(new ReporteArgentina("ARG-4-006", new double[] {10,7,1,2,1,1,10,10,8,4}));
		this.reportes.add(new ReporteArgentina("ARG-3-553", new double[] {2,10,9,3,9,7,7,1,3,1}));
		this.reportes.add(new ReporteArgentina("ARG-1-729", new double[] {6,3,6,3,3,3,6,1,3,5}));
		this.reportes.add(new ReporteArgentina("ARG-2-150", new double[] {4,7,8,5,6,4,6,5,2,1}));
		this.reportes.add(new ReporteArgentina("ARG-2-846", new double[] {9,8,3,2,9,9,6,4,8,3}));
		this.reportes.add(new ReporteArgentina("ARG-3-197", new double[] {7,8,1,3,8,8,10,9,2,4}));
		this.reportes.add(new ReporteArgentina("ARG-2-946", new double[] {7,8,10,7,10,8,8,6,9,7}));
		this.reportes.add(new ReporteColombia("COL-1-483", new double[] {4.3,4.0,2.3,4.5,1.3,1.0,1.9,1.0,4.2,4.0},false));
		this.reportes.add(new ReporteColombia("COL-1-549", new double[] {4.0,4.4,3.3,1.0,4.9,1.2,1.9,0.6,3.3,1.5},true));
		this.reportes.add(new ReporteColombia("COL-1-497", new double[] {1.3,2.7,4.0,0.5,3.4,2.5,4.3,1.3,4.9,4.1},false));
		this.reportes.add(new ReporteColombia("COL-1-616", new double[] {5.0,1.2,0.5,4.1,4.1,2.2,1.0,3.7,3.9,4.4},false));
		this.reportes.add(new ReporteColombia("COL-1-110", new double[] {2.4,2.1,1.7,4.6,4.1,2.6,1.2,2.8,1.5,3.5},false));
		this.reportes.add(new ReporteColombia("COL-1-419", new double[] {3.1,0.8,2.9,1.8,5.0,4.9,2.6,0.9,2.9,2.7},false));
		this.reportes.add(new ReporteColombia("COL-1-289", new double[] {3.2,1.7,1.7,3.5,2.1,5.0,1.1,2.1,1.9,0.6},false));
		this.reportes.add(new ReporteColombia("COL-1-424", new double[] {3.6,2.6,3.6,0.7,0.9,4.1,1.9,2.7,4.6,4.7},false));
		this.reportes.add(new ReporteColombia("COL-1-563", new double[] {3.3,4.5,1.2,0.6,4.9,1.2,3.6,1.6,3.4,1.2},false));
		this.reportes.add(new ReporteColombia("COL-1-152", new double[] {4.3,4.2,4.3,4.8,4.4,4.7,4.8,4.6,4.2,4.5},true));
		this.reportes.add(new ReporteColombia("COL-0-885", new double[] {2.6,2.0,1.0,3.5,2.2,2.7,3.4,4.0,2.5,3.5},false));
		this.reportes.add(new ReporteColombia("COL-1-367", new double[] {1.6,2.6,3.3,1.3,2.0,4.6,0.8,4.3,2.5,0.7},false));
		this.reportes.add(new ReporteColombia("COL-1-268", new double[] {4.4,3.3,0.6,1.4,4.4,2.9,3.3,1.7,2.1,2.0},true));
		this.reportes.add(new ReporteColombia("COL-1-441", new double[] {0.5,4.8,3.4,4.3,3.6,2.8,0.7,2.7,4.4,2.8},false));
	}
}
